var isAbsoluteUrl = require('is-absolute-url')
var Url = require('url')
var normalizeUrl = require('normalize-url')
var hash = require('hash-string')

var fixUrl = function (url, link) {
  var mainUrl = Url.parse(link.url).protocol + '//' + Url.parse(link.url).hostname
  if (isAbsoluteUrl(url)) { return normalizeUrl(url) } else {
    return normalizeUrl(Url.resolve(mainUrl, url))
  }
}

var fixImages = function (images, link) {
  images.map(function (image) {
    var img = fixUrl(image.img.toString(), link)
    var url = fixUrl(image.url.toString(), link)
    image.url = url
    image.img = img
    image.hash = hash(img + url + image.user)
    return image
  })
}

var fixCars = function (cars, link) {
  cars.map(function (car) {
    var img = fixUrl(car.img.toString(), link)
    var url = fixUrl(car.url.toString(), link)
    car.url = url
    car.img = img
    car.hash = hash(img + url + car.user)
    return car
  })
}

module.exports = {
  fixImages: fixImages,
  fixCars: fixCars
}
