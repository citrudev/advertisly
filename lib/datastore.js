var Links = require('../models/link')
var Cars = require('../models/cars')
var Images = require('../models/images')
var appDebug = require('debug')('loader.js')

var _ = require('underscore')

var loadLinks = function (cb) {
  Links.find({}, { comment: 0, __v: 0 }, function (err, links) {
    if (!err && links) {
      cb(null, _.shuffle(links))
    } else {
      appDebug('error: ' + err)
      cb(err)
    }
  })
}

var addImages = function (images) {
  images.forEach(function (image) {
    if (image.img && image.url) {
      var newImage = new Images()
      newImage.hash = image.hash
      newImage.img = image.img
      newImage.url = image.url
      newImage.link = image.link
      newImage.user = image.user
      newImage.save(function (err) { if (err) console.log(err) })
    }
  })
}

var addCars = function (cars) {
  cars.forEach(function (car) {
    var newCar = new Cars()
    newCar.url = car.url
    newCar.hash = car.hash
    newCar.user = car.user
    newCar.link = car.link
    newCar.img = car.img
    newCar.make = car.make
    newCar.model = car.model
    newCar.trim = car.trim
    newCar.year = car.year
    newCar.lease = car.lease
    newCar.finance = car.finance
    newCar.msrp = car.msrp
    newCar.save()
  })
}

var deleteAllImages = function () {
  Images.remove({}, function (err) {
    if (err) console.log(err)
  })
}

var deleteAllCars = function () {
  Cars.remove({}, function (err) {
    if (err) console.log(err)
  })
}

var deletAllCarsLink = function (link) {
  Cars.remove({link: link._id}, function (err) {
    if (err) console.log(err)
  })
}

var deleteAllImagesLink = function (link) {
  Images.remove({link: link._id}, function (err) {
    if (err) console.log(err)
  })
}

module.exports = {
  loadLinks: loadLinks,
  addImages: addImages,
  addCars: addCars,
  deleteAllImages: deleteAllImages,
  deleteAllCars: deleteAllCars,
  deletAllCarsLink: deletAllCarsLink,
  deleteAllImagesLink: deleteAllImagesLink
}
