
module.exports = function (Nightmare) {
  return new Nightmare(
    {
      show: false,
      maxAuthRetries: 10,
      waitTimeout: 100000,
      electronPath: require('electron'),
      switches: {
        'ignore-certificate-errors': true
      },
      webPreferences: {
        webSecurity: false
      }
    })
}
