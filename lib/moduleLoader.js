var plugins = require('require-dir-all')('../plugins')
var appDebug = require('debug')('moduleLoader')
var checkPlugin = require('../plugin_helper/checkPlugin')

var getScrapePluginFn = function (link) {
  var pluginFn = link.url
  var log = pluginFn + ' ' + 'plugin scrape function not found'
  for (var plugin in plugins) {
    if (plugins[plugin][pluginFn]) {
      appDebug(pluginFn + ' plugin scrape function found')
      checkPlugin(link)
      return plugins[plugin][pluginFn].scrape
    }
  }
  appDebug(log)
  return function * (val) {
    return {
      log: [log],
      link: val
    }
  }
}
module.exports = {
  getScrapePluginFn: getScrapePluginFn
}
