// scaping plugin for lutherbrookdalehonda
// =====================================START=========================================

var nightmare = require('../lib/configuredNightmare')(require('nightmare'))
var datastore = require('../lib/datastore')
var utility = require('../lib/utility')
var log = []
var pluginSwitch = {}
pluginSwitch['on'] = 'http://www.walserhonda.com/new-honda-sign-and-drive-lease-offers-near-minneapolis-mn.htm'
pluginSwitch['off'] = null

var scrape = function * (link) {
  var cars = yield nightmare.goto('http://www.walserhonda.com/new-honda-sign-and-drive-lease-offers-near-minneapolis-mn.htm')
  .wait()
  .inject('js', 'node_modules/jquery/dist/jquery.js')
  .evaluate(function (link) {
    var cars = []
    var count = 0
    $('.car-box-container-main').each(function () {
      count++
      var carObj = {}
      var title = $(this).find('.walser-car-title-container-main').text()
      var year = title.toString().split(' ')[0]
      var model = title.toString().split(' ')[1]
      var trim = title.toString().split(' ').reduce(function (total, currentValue, index, arr) {
        return total + index > 1 ? currentValue + ' ' : ''
      })
      var lease = $(this).find('.rates-box-container-main .large-text').text()
      var img = $(this).find('.image-box-container-main img').attr('src')
      var url = $(this).find('.btn-container-box a:nth-child(2)').attr('href')
      carObj.user = link.user
      carObj.link = link._id
      carObj.img = img.toString().trim()
      carObj.url = url.toString().trim()
      carObj.make = 'Honda'
      carObj.year = year.toString().trim()
      carObj.model = model.toString().trim()
      carObj.trim = trim.toString().trim()
      carObj.finance = ''
      carObj.msrp = ''
      carObj.lease = lease.toString().trim()
      carObj.count = count
      cars.push(carObj)
    })
    return cars
  }, link).end()
  utility.fixCars(cars, link)
  datastore.addCars(cars)
  return {
    log: log,
    link: link
  }
}
// 'http://www.walserhonda.com/new-honda-sign-and-drive-lease-offers-near-minneapolis-mn.htm'
// =======================================================PLUGIN====================================================
module.exports[pluginSwitch['on']] = {
  scrape: scrape
}
