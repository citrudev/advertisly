// scaping plugin for lutherbrookdalehonda
// =====================================START=========================================

var nightmare = require('../lib/configuredNightmare')(require('nightmare'))
var datastore = require('../lib/datastore')
var utility = require('../lib/utility')
var log = []
var pluginSwitch = {}
pluginSwitch['on'] = 'http://www.lutherbrookdalehonda.com/true-zero-leasing.html'
pluginSwitch['off'] = null

var scrape = function * (link) {
  var images = yield nightmare.goto('http://www.lutherbrookdalehonda.com/true-zero-leasing.html')
  .wait()
  .inject('js', 'node_modules/jquery/dist/jquery.js')
  .evaluate(function (link) {
    var images = []
    $('.LeasePage .col-md-12').each(function () {
      var imageObj = {}
      imageObj.url = $(this).find('a').attr('href')
      imageObj.img = $(this).find('img').attr('src')
      imageObj.link = link._id
      imageObj.user = link.user
      if (imageObj.url && imageObj.img) {
        images.push(imageObj)
      }
    })
    return images
  }, link).end()
  utility.fixImages(images, link)
  datastore.addImages(images)
  return {
    log: log,
    link: link
  }
}


// =======================================================PLUGIN====================================================
module.exports[pluginSwitch['on']] = {
  scrape: scrape
}
