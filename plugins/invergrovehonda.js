// scaping plugin for lutherbrookdalehonda
// =====================================START=========================================

var nightmare = require('../lib/configuredNightmare')(require('nightmare'))
var datastore = require('../lib/datastore')
var utility = require('../lib/utility')
var log = []
var pluginSwitch = {}
pluginSwitch['on'] = 'http://www.invergrovehonda.com/new-vehicles/new-vehicle-specials'
pluginSwitch['off'] = null

var scrape = function * (link) {
  var images = yield nightmare.goto('http://www.invergrovehonda.com/new-vehicles/new-vehicle-specials')
    .wait()
    .inject('js', 'node_modules/jquery/dist/jquery.js')
    .evaluate(function (link) {
      var images = []
      $('.visible-lg, .visible-sm').each(function () {
        var imageObj = {}
        imageObj.url = $(this).find('a').attr('href')
        imageObj.img = $(this).find('a img').attr('src')
        imageObj.link = link._id
        imageObj.user = link.user
        if (imageObj.url && imageObj.img) {
          images.push(imageObj)
        }
      })
      return images
    }, link).end()
  utility.fixImages(images, link)
  datastore.addImages(images)
  return {
    log: log,
    link: link
  }
}

/* 'http://www.invergrovehonda.com/new-vehicles/new-vehicle-specials' */
// =======================================================PLUGIN====================================================

module.exports[pluginSwitch['on']] = {
  scrape: scrape
}
