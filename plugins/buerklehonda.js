// scaping plugin for lutherbrookdalehonda
// =====================================START=========================================

var nightmare = require('../lib/configuredNightmare')(require('nightmare'))
var datastore = require('../lib/datastore')
var utility = require('../lib/utility')
var log = []
var pluginSwitch = {}
pluginSwitch['on'] = 'https://www.buerklehonda.com/specials/new.htm'
pluginSwitch['off'] = null

var scrape = function * (link) {
  var images = yield nightmare.goto('https://www.buerklehonda.com/specials/new.htm')
  .wait()
  .inject('js', 'node_modules/jquery/dist/jquery.js')
  .evaluate(function (link) {
    var images = []
    $('.dsbCont').each(function () {
      var imageObj = {}
      imageObj.url = $(this).find('a').attr('href')
      imageObj.img = $(this).find('a img').attr('src')
      imageObj.link = link._id
      imageObj.user = link.user
      if (imageObj.url && imageObj.img) {
        images.push(imageObj)
      }
    })
    return images
  }, link).end()

  utility.fixImages(images, link)
  datastore.addImages(images)
  return {
    log: log,
    link: link
  }
}
// 'https://www.buerklehonda.com/specials/new.htm'
// =======================================================PLUGIN====================================================
module.exports[pluginSwitch['on']] = {
  scrape: scrape
}
