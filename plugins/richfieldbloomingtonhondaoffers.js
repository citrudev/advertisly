// scaping plugin for richfieldbloomingtonhondaoffers
// =====================================START=========================================

var nightmare = require('../lib/configuredNightmare')(require('nightmare'))
var datastore = require('../lib/datastore')
var utility = require('../lib/utility')
var log = []
var pluginSwitch = {}
pluginSwitch['on'] = 'http://richfieldbloomingtonhondaoffers.com/specialoffers/vehiclespecials'
pluginSwitch['off'] = null

var scrape = function * (link) {
  yield nightmare.goto('http://richfieldbloomingtonhondaoffers.com/specialoffers/vehiclespecials')
  var previousHeight
  var currentHeight = 0

  while (previousHeight !== currentHeight) {
    previousHeight = currentHeight
    var currentHeight = yield nightmare.evaluate(function () {
      return document.body.scrollHeight
    })
    yield nightmare.scrollTo(currentHeight, 0).wait(3000)
  }

  var cars = yield nightmare.inject('js', 'node_modules/jquery/dist/jquery.js')
    .evaluate(function (link) {
      var cars = []
      $('.NVContainer').each(function () {
        var carObj = {}
        var year = $(this).find('.showVeh .NVTextContainerBig .NVTextContainerSmall .page2offerText:nth-child(1)').text()
        year = year.toString().split(' ')[2]
        var details = $(this).find('.showVeh .NVTextContainerSmall .page2OfferPrice').text()
        var lease = details.toString().split(' ')[details.toString().split(' ').length - 2]
        var model = details.toString().split(' ')[0]
        var trim = details.toString().split(' ')[1]
        var img = $(this).find('.showVeh .specialNVCarContainer img').attr('src')
        var url = $(this).find('.showVeh .NVButtonsContainer a:nth-child(3)').attr('href')
        carObj.user = link.user
        carObj.link = link._id
        carObj.img = img.toString().trim()
        carObj.url = url.toString().trim()
        carObj.make = 'Honda'
        carObj.year = year
        carObj.model = model.trim()
        carObj.trim = trim.trim()
        carObj.finance = ''
        carObj.msrp = ''
        carObj.lease = lease
        cars.push(carObj)
      })
      return cars
    }, link).end()
  utility.fixCars(cars, link)
  datastore.addCars(cars)
  return {
    log: log,
    link: link
  }
}
// 'http://richfieldbloomingtonhondaoffers.com/specialoffers/vehiclespecials'
// =======================================================PLUGIN====================================================
module.exports[pluginSwitch['on']] = {
  scrape: scrape
}
