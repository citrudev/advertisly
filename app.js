require('dotenv').config()
var express = require('express')
var mongoose = require('mongoose')
var passport = require('passport')
var bodyParser = require('body-parser')
var cookieParser = require('cookie-parser')
var session = require('express-session')
var helmet = require('helmet')
var path = require('path')
var Redisstore = require('connect-redis')(session)
var User = require('./models/user')
var auth = require('./config/auth')(passport)
var appDebug = require('debug')('app.js')
var app = express()
var server = require('http').Server(app)
var io = require('socket.io')(server)
var redis = require('redis')
var flash = require('connect-flash')
var client = redis.createClient()
var publisher = redis.createClient()

appDebug.log = console.log.bind(console)
mongoose.connect(require('./config/database').url)
// create default admin account if one is not found
User.findOne({email: process.env.ADMIN_EMAIL}, function (err, admin) {
  if (!(!err && admin)) {
    var newAdmin = new User()
    newAdmin.email = process.env.ADMIN_EMAIL
    newAdmin.dealership = process.env.DEALER
    newAdmin.password = newAdmin.generateHash(process.env.ADMIN_PASS)
    newAdmin.isAdmin = true
    newAdmin.verified = true
    newAdmin.save()
  }
})

app.engine('html', require('hbs').__express)
app.set('views', path.resolve(__dirname, 'app'))
app.set('view engine', 'html')
app.use(flash())
app.use(express.static('.tmp'))
app.use('/bower_components', express.static('bower_components'))
app.use(express.static('app'))
app.use(express.static('app/theme/docs'))
app.use('/clients', express.static('app/theme/docs'))
app.use(helmet())
app.use(helmet.frameguard({ action: 'sameorigin' }))
app.use(helmet.dnsPrefetchControl())
app.use(helmet.hidePoweredBy())
app.use(helmet.ieNoOpen())
app.use(helmet.noSniff())
app.use(helmet.xssFilter())
app.use(bodyParser.json({extended: true})) // get JSON data
app.use(bodyParser.urlencoded({extended: true}))
app.use(cookieParser('S3CRE7kil78904lj4jhl4386'))
app.use(session({
  secret: 'ilovescotchscotchyscotchscotch',
  // create new redis store.
  store: new Redisstore({host: 'localhost', port: 6379, client: client, ttl: 760}),
  saveUninitialized: false,
  resave: false
}))
app.use(passport.initialize())
app.use(passport.session())

require('./gulpfile')()
require('./routes')(app, auth)
require('./comms')(io)

setTimeout(function () {
  publisher.publish('startmonitor', JSON.stringify({interval: 3600000, timeout: 100000, lpi: 1}))
}, 10000)

server.listen(process.env.PORT, process.env.IP, function () {
  appDebug('sever started on this' + process.env.IP + ':' + process.env.PORT)
})
