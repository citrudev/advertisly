var appDebug = require('debug')('startMonitor')

var startMonitor = function (message) {
  console.log('got to the xvfb function')
  message = JSON.parse(message)
  var Xvfb = require('xvfb')
  var xvfb = new Xvfb({reuse: true})
  xvfb.start(function (err, xvfbProcess) {
    if (err) appDebug('error %O', err)
    require('../scheduler').startUp(require('../lib/datastore'))
    require('../scheduler')(message.interval, message.timeout, message.lpi, require('../lib/datastore'))
  })
}


module.exports = startMonitor

