var redis = require('redis')
var subscriber = redis.createClient()
var commsHandler = require('./commsHandler')
var appDebug = require('debug')('comms')

appDebug('comms loaded')

module.exports = function (io) {
  io.on('connection', function (socket) {
    subscriber.on('message', function (channel, message) {
      appDebug('channel called: ' + channel)
      socket.emit(channel, message)
    })
  })

  subscriber.on('message', function (channel, message) {
    console.log('works')
    appDebug('channel called: ' + channel)
    if (typeof commsHandler[channel] === 'function') {
      commsHandler[channel](message)
    }
  })


  subscriber.subscribe('startmonitor')
}
