var mongoose = require('mongoose')

var linkSchema = mongoose.Schema({
  url: { type: String },
  user: {type: mongoose.Schema.Types.ObjectId, ref: 'user'},
  comment: {type: String},
  plugin: {type: Boolean, default: false}
})

module.exports = mongoose.model('link', linkSchema)

