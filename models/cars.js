var mongoose = require('mongoose')

var carsSchema = mongoose.Schema({
  hash: {type: String, unique: true},
  link: {type: mongoose.Schema.Types.ObjectId, ref: 'link'},
  user: {type: mongoose.Schema.Types.ObjectId, ref: 'user'},
  url: {type: String},
  img: {type: String},
  make: {type: String},
  model: {type: String},
  trim: {type: String},
  year: {type: String},
  lease: {type: String},
  finance: {type: String},
  msrp: {type: String},
  createdAt: { type: Date, expires: 604800, default: Date.now }
})

module.exports = mongoose.model('cars', carsSchema)
