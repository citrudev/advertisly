var mongoose = require('mongoose')

var ImagesSchema = mongoose.Schema({
  hash: {type: String, unique: true, sparse: true},
  img: {type: String, sparse: true},
  url: {type: String, sparse: true},
  link: {type: mongoose.Schema.Types.ObjectId, ref: 'link'},
  user: {type: mongoose.Schema.Types.ObjectId, ref: 'user'},
  extracted: {type: Boolean, default: false},
  createdAt: { type: Date, expires: 604800, default: Date.now }
})

module.exports = mongoose.model('images', ImagesSchema)
