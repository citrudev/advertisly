angular.module('MaterialApp').directive('userpriv', function ($http, $state, $window) {
  return {
    restrict: 'E',
    templateUrl: 'scripts/directives/adminUsers/usersprivelege.html?v=' + window.app_version,
    replace: true,
    scope: {
      verified: '=',
      isCrowdWorker: '=',
      isAdmin: '=',
      dealership: '@',
      email: '@',
      id: '@'
    },
    link: function (scope) {
      scope.setAdmin = function () {
        $http({
          method: 'POST',
          url: '/updateUserPrivilege',
          data: { id: scope.id, isAdmin: scope.isAdmin }
        }).then(function (response) {
          if (response.status !== 200) {
            $window.location.reload()
          }
        })
      }

      scope.setCrowdWorker = function () {
        $http({
          method: 'POST',
          url: '/updateUserPrivilege',
          data: { id: scope.id, isCrowdWorker: scope.isCrowdWorker }
        }).then(function (response) {
          if (response.status !== 200) {
            $window.location.reload()
          }
        })
      }

      scope.setVerified = function () {
        $http({
          method: 'POST',
          url: '/updateUserPrivilege',
          data: { id: scope.id, verified: scope.verified }
        }).then(function (response) {
          if (response.status !== 200) {
            $window.location.reload()
          }
        })
      }

      scope.deleteUser = function () {
        var isDelete = confirm('Do you want to delete this record?')
        if (isDelete) {
          $http({
            method: 'POST',
            url: '/deleteUser',
            data: {id: scope.id}
          }).then(function (response) {
            if (response.status === 200) {
              $state.reload()
            } else {
              $window.location.reload()
            }
          })
        }
      }
    }
  }
})
