angular.module('MaterialApp').directive('linkCard', function ($http, $state, $window) {
  return {
    restrict: 'E',
    templateUrl: 'scripts/directives/listCars/listLinks.html?v=' + window.app_version,
    replace: true,
    scope: {
      url: '@',
      comment: '=',
      plugin: '@',
      id: '@'
    },
    link: function (scope) {
      scope.updateComment = function () {
        $http({
          method: 'POST',
          url: '/updateLinkComment',
          data: { id: scope.id, comment: scope.comment}
        }).then(function (response) {
          if (response.status === 200) {
            $state.reload()
          }
        })
      }

      scope.deleteLink = function () {
        $http({
          method: 'POST',
          url: '/deleteLink',
          data: {id: scope.id}
        }).then(function (response) {
          if (response.status === 200) {
            $state.reload()
          }
        })
      }
    }
  }
})
