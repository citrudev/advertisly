angular.module('MaterialApp').directive('workinfo', function ($http, $state, $window) {
  return {
    restrict: 'E',
    templateUrl: 'scripts/directives/listCars/workInfo.html?v=' + window.app_version,
    replace: true,
    scope: {
      id: '@',
      url: '@',
      link: '@',
      user: '@',
      img: '@',
      hash: '@',
      make: '=',
      model: '=',
      trim: '=',
      year: '=',
      lease: '=',
      finance: '=',
      msrp: '='
    },
    link: function (scope) {
      scope.submitInfo = function () {
        var car = {url: scope.url || '', link: scope.link || '', user: scope.user || '', img: scope.img || '', make: scope.make || '', model: scope.model || '', trim: scope.trim || '', year: scope.year || '', lease: scope.lease || '', finance: scope.finance || '', msrp: scope.msrp || '', hash: scope.hash || ''}
        var imgHash = {hash: scope.hash}
        $http({
          method: 'POST',
          url: '/addCar',
          data: car
        }).then(function (response) {
          if (response.status === 200) {
            $http({
              method: 'POST',
              url: '/setImageExtracted',
              data: imgHash
            }).then(function (response) {
              if (response.status === 200) {
                $state.reload()
              }
            })
          }
        })
      }


      scope.hideInfo = function () {
        var data = {hash: scope.hash}
        $http({
          method: 'POST',
          url: '/setImageExtracted',
          data: data
        }).then(function (response) {
          if (response.status === 200) {
            $state.reload()
          }
        })
      }
    }
  }
})
