angular.module('MaterialApp').directive('listcar', function ($http, $state, $window) {
  return {
    restrict: 'E',
    templateUrl: 'scripts/directives/listCars/listCars.html?v=' + window.app_version,
    replace: true,
    scope: {
      url: '@',
      img: '@',
      make: '@',
      model: '@',
      trim: '@',
      year: '@',
      lease: '@',
      finance: '@',
      msrp: '@'
    }
  }
})
