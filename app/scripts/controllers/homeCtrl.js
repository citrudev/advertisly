'use strict'
/**
 * @ngdoc function
 * @name MaterialApp.controller:HomeCtrl
 * @description
 * # HomeCtrl
 * Controller of MaterialApp
 */
var homeapp = angular.module('MaterialApp')

homeapp.controller('HomeCtrl', ['$scope', '$timeout', '$http', '$state', '$window', function ($scope, $timeout, $http, $state, $window) {
  $scope.queryCars = function () {
    $http({
      method: 'POST',
      url: '/getAllCars',
      data: { query: $scope.query }
    }).then(function (response) {
      if (response.status === 200) {
        $scope.carList = response.data
      } else {
        $window.location.reload(true)
      }
    })
  }

  $scope.queryCars()

  $scope.criteriaMatch = function (queries) {
    var compare = function (item) {
      var testAll = []

      if (queries) {
        testAll = queries.split(' ').map(function (query) {
          if (!(query.split(/[=><]+/).length > 1)) {
            var regExp1 = new RegExp(query, 'ig')
            return [ regExp1.test(item['make']), regExp1.test(item['model']), regExp1.test(item['trim']), regExp1.test(item['lease']), regExp1.test(item['finance']), regExp1.test(item['msrp']), regExp1.test(item['year']), regExp1.test(item['url'])].some(function (val) {
              return val
            })
          } else {
            console.log(query)
            if (query.split('>').length > 1) {
              var field2 = query.split('>')
              if (field2[0].toLowerCase() === 'lease') {
                var val = Number(item[field2[0].toLowerCase()].split('$').join(''))
                return val > Number(field2[1])
              } else {
                return Number(item[field2[0].toLowerCase()]) > Number(field2[1])
              }
            } else if (query.split('<').length > 1) {
              var field3 = query.split('<')

              if (field3[0].toLowerCase() === 'lease') {
                var val1 = Number(item[field3[0].toLowerCase()].toLowerCase().split('$').join(''))
                return val1 < Number(field3[1])
              } else {
                return Number(item[field3[0].toLowerCase()]) < Number(field3[1])
              }
            } else if (query.split('=').length > 1) {
              var field = query.split('=')
              if (isNaN(item[field[0].toLowerCase()]) && typeof item[field[0].toLowerCase()] === 'string') {
                var regExp2 = new RegExp(field[1], 'ig')
                return regExp2.test(item[field[0].toLowerCase()])
              } else {
                return item[field[0].toLowerCase()] == field[1]
              }
            }

            return false
          }
        })
      }

      return testAll.every(function (val) {
        return val
      })
    }
    return function (item) {
      return compare(item)
    }
  }
}])
