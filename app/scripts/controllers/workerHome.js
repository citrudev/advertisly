'use strict'

/**
 * @ngdoc function
 * @name MaterialApp.controller:HomeCtrl
 * @description
 * # HomeCtrl
 * Controller of MaterialApp
 */
angular.module('MaterialApp').controller('workerHomeCtrl', ['$scope', '$timeout', '$http', '$window', function ($scope, $timeout, $http, $window) {
  $http({
    method: 'GET',
    url: '/getAllImages'
  }).then(function (response) {
    if (response.status === 200) {
      $scope.images = response.data
    } else {
      $window.location.reload(true)
    }
  })
}])
