angular.module('MaterialApp').controller('adminUsersCtrl', ['$scope', '$http', '$window', function ($scope, $http, $window) {
  $http.get('/getAllUsers')
    .then(function (response) {
      if (response.status === 200) {
        console.log(response.data)
        $scope.users = response.data
      } else {
        $window.location.reload(true)
      }
    })

  $scope.getUsers = function () {
    $http.get('/getAllUsers')
    .then(function (response) {
      if (response.status === 200) {
        console.log(response.data)
        $scope.users = response.data
      } else {
        $window.location.reload(true)
      }
    })
  }
}])
