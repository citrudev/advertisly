'use strict'

/**
 * @ngdoc function
 * @name MaterialApp.controller:HomeCtrl
 * @description
 * # HomeCtrl
 * Controller of MaterialApp
 */
angular.module('MaterialApp').controller('adminHomeCtrl', ['$scope', '$timeout', '$http', '$window', function ($scope, $timeout, $http, $window) {
  $scope.flushCars = function () {
    $http.get('/deleteAllCars').then(function (response) {
      if (response.status !== 200) {
        $window.location.reload(true)
      } else {
        alert('database has been flushed')
      }
    })
  }

  $scope.flushImages = function () {
    $http.get('/deleteAllImages').then(function (response) {
      if (response.status !== 200) {
        $window.location.reload(true)
      } else {
        alert('database has been flushed')
      }
    })
  }
}])
