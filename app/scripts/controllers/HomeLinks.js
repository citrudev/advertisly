angular.module('MaterialApp')
  .controller('HomeLinksCtrl', function ($scope, $http, $state, $window) {
    $http.get('/getAllLinks')
    .then(function (response) {
      if (response.status === 403) {
        $window.location.href = '/logout'
      }
      if (response.status === 200) {
        $scope.links = response.data
      }
    })

    $scope.addLink = function () {
      if ($scope.url) {
        var data = { url: $scope.url, comment: $scope.comment || '' }
        console.log(data)
        $http({
          method: 'POST',
          url: '/addLink',
          data: data
        }).then(function (response) {
          if (response.status === 200) {
            $state.reload()
          }
          if (response.status === 403) {
            $window.location.href = '/logout'
          }
        })
      } else {
        alert('Input Url')
      }
    }
  })
