'use strict'

/**
* @ngdoc overview
* @name MaterialApp
* @description
* # MaterialApp
*
* Main module of the application.
*/
window.app_version = 2.0

angular
.module('MaterialApp', [
  'ui.router',
  'ngAnimate',
  'ngMaterial',
  'chart.js',
  'gridshore.c3js.chart',
  'angular-growl',
  'growlNotifications',
  'angular-loading-bar',
  'easypiechart',
  'ui.sortable',
  angularDragula(angular),
  'bootstrapLightbox',
  'materialCalendar',
  'paperCollapse',
  'pascalprecht.translate'
])
    .config(['cfpLoadingBarProvider', function (cfpLoadingBarProvider) {
      cfpLoadingBarProvider.latencyThreshold = 5
      cfpLoadingBarProvider.includeSpinner = false
    }])

    .config(function ($translateProvider) {
      $translateProvider.useStaticFilesLoader({
        prefix: 'languages/',
        suffix: '.json'
      })
      $translateProvider.useSanitizeValueStrategy(null)
      $translateProvider.preferredLanguage('en')
    })

    .config(function ($stateProvider, $urlRouterProvider, $locationProvider) {
      $urlRouterProvider.when('/admin', '/admin/home')

      $stateProvider
    .state('base', {
      abstract: true,
      url: '',
      templateUrl: 'views/base.html?v=' + window.app_version,
      controller: 'adminDashboardCtrl'
    })
    .state('admin', {
      url: '/admin',
      parent: 'base',
      templateUrl: 'views/layouts/adminDashboard.html?v=' + window.app_version,
      controller: 'adminDashboardCtrl'
    })
    .state('home', {
      url: '/home',
      parent: 'admin',
      templateUrl: 'views/pages/dashboard/adminHome.html?v=' + window.app_version,
      controller: 'adminHomeCtrl'
    })
    .state('users', {
      url: '/users',
      parent: 'admin',
      templateUrl: 'views/pages/dashboard/adminUsers.html?v=' + window.app_version,
      controller: 'adminUsersCtrl'
    })
      $locationProvider.html5Mode(true)
    })

