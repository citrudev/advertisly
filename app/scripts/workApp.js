'use strict'

/**
* @ngdoc overview
* @name MaterialApp
* @description
* # MaterialApp
*
* Main module of the application.
*/
window.app_version = 2.0

angular
.module('MaterialApp', [
  'ui.router',
  'ngAnimate',
  'ngMaterial',
  'chart.js',
  'gridshore.c3js.chart',
  'angular-growl',
  'growlNotifications',
  'angular-loading-bar',
  'easypiechart',
  'ui.sortable',
  angularDragula(angular),
  'bootstrapLightbox',
  'materialCalendar',
  'paperCollapse',
  'pascalprecht.translate'
])
    .config(['cfpLoadingBarProvider', function (cfpLoadingBarProvider) {
      cfpLoadingBarProvider.latencyThreshold = 5
      cfpLoadingBarProvider.includeSpinner = false
    }])

    .config(function ($translateProvider) {
      $translateProvider.useStaticFilesLoader({
        prefix: 'languages/',
        suffix: '.json'
      })
      $translateProvider.useSanitizeValueStrategy(null)
      $translateProvider.preferredLanguage('en')
    })

    .config(function ($stateProvider, $urlRouterProvider, $locationProvider) {
      $urlRouterProvider.when('/workspace', '/dashboard/workspace')
      $stateProvider
    .state('base', {
      abstract: true,
      url: '',
      templateUrl: 'views/base.html?v=' + window.app_version,
      controller: 'DashboardCtrl'
    })
    .state('dashboard', {
      url: '/dashboard',
      parent: 'base',
      templateUrl: 'views/layouts/workerdashboard.html?v=' + window.app_version,
      controller: 'DashboardCtrl'
    })
    .state('workspace', {
      url: '/workspace',
      parent: 'dashboard',
      templateUrl: 'views/pages/dashboard/work.html?v=' + window.app_version,
      controller: 'workerHomeCtrl'
    })
      $locationProvider.html5Mode(true)
    })

