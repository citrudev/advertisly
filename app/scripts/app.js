'use strict'

/**
* @ngdoc overview
* @name MaterialApp
* @description
* # MaterialApp
*
* Main module of the application.
*/
window.app_version = 2.0

angular
.module('MaterialApp', [
  'ui.router',
  'ngAnimate',
  'ngMaterial',
  'chart.js',
  'gridshore.c3js.chart',
  'angular-growl',
  'growlNotifications',
  'angular-loading-bar',
  'easypiechart',
  'ui.sortable',
  angularDragula(angular),
  'bootstrapLightbox',
  'materialCalendar',
  'paperCollapse',
  'pascalprecht.translate'
])
    .config(['cfpLoadingBarProvider', function (cfpLoadingBarProvider) {
      cfpLoadingBarProvider.latencyThreshold = 5
      cfpLoadingBarProvider.includeSpinner = false
    }])

    .config(function ($translateProvider) {
      $translateProvider.useStaticFilesLoader({
        prefix: 'languages/',
        suffix: '.json'
      })
      $translateProvider.useSanitizeValueStrategy(null)
      $translateProvider.preferredLanguage('en')
    })

    .config(function ($stateProvider, $urlRouterProvider, $locationProvider) {
      $urlRouterProvider.when('/home', '/dashboard/home')
      $stateProvider
    .state('base', {
      abstract: true,
      url: '',
      templateUrl: 'views/base.html?v=' + window.app_version,
      controller: 'DashboardCtrl'
    })
    .state('dashboard', {
      url: '/dashboard',
      parent: 'base',
      templateUrl: 'views/layouts/dashboard.html?v=' + window.app_version,
      controller: 'DashboardCtrl'
    })
    .state('home', {
      url: '/home',
      parent: 'dashboard',
      templateUrl: 'views/pages/dashboard/home.html?v=' + window.app_version,
      controller: 'HomeCtrl'
    })
    .state('links', {
      url: '/links',
      parent: 'dashboard',
      templateUrl: 'views/pages/dashboard/links.html?v=' + window.app_version,
      controller: 'HomeLinksCtrl'
    })

      $locationProvider.html5Mode(true)
    })

