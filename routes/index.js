var mainCtrl = require('../controllers/main')
var appDebug = require('debug')('route')

appDebug('routes loaded')

module.exports = function (app, auth) {
  app.get('/', mainCtrl.landingView)

  app.get('/clients/:plan', mainCtrl.clientView)

  app.get('/login', mainCtrl.loginView)

  app.get('/signup', mainCtrl.signupView)

  app.get(['/home', '/dashboard/home'], auth.isAuthenticated, auth.requireLogin, auth.isAdmin, auth.isCrowdWorker, mainCtrl.homeView)

  app.get(['/workspace', '/dashboard/workspace'], auth.isAuthenticated, auth.requireLogin, auth.workerApi, mainCtrl.workspaceView)

  app.get(['/admin', '/admin/home', '/admin/users'], auth.isAuthenticated, auth.requireLogin, auth.adminApi, mainCtrl.adminView)

  app.get('/logout', auth.logout)

  app.get('/signupStatus', mainCtrl.signupStatusView)

  app.get('/signupFailed', mainCtrl.signupFailedView)

  app.get('/signinFailed', mainCtrl.signinFailedView)

  app.get('/forgot', mainCtrl.forgotView)

  app.get('/getCurrentUser', auth.apiRequireLogin, mainCtrl.getCurrentUser)

  app.get('/getAllUsers', auth.apiRequireLogin, auth.adminApi, mainCtrl.getAllUsers)

  app.get('/getAllLinks', auth.apiRequireLogin, mainCtrl.getAllLinks)

  app.get('/getAllImages', auth.apiRequireLogin, mainCtrl.getAllImages)

  app.get('/deleteAllImages', auth.apiRequireLogin, mainCtrl.deleteAllImages)

  app.get('/deleteAllCars', auth.apiRequireLogin, mainCtrl.deleteAllCars)
// ------------------------------------------------------------POST---------------------------------------------------------------------

  app.post('/getAllCars', auth.apiRequireLogin, mainCtrl.getAllCars)

  app.post('/setImageExtracted', auth.apiRequireLogin, auth.workerApi, mainCtrl.setImageExtracted)

  app.post('/addCar', auth.apiRequireLogin, auth.workerApi, mainCtrl.addCar)

  app.post('/login', auth.login)

  app.post('/signup', auth.register)

  app.post('/deleteUser', auth.apiRequireLogin, auth.adminApi, mainCtrl.deleteUser)

  app.post('/updateUserPrivilege', auth.apiRequireLogin, auth.adminApi, mainCtrl.updateUserPrivilege)

  app.post('/addLink', auth.apiRequireLogin, mainCtrl.addLink)

  app.post('/updateLinkComment', auth.apiRequireLogin, mainCtrl.updateLinkComment)

  app.post('/deleteLink', auth.apiRequireLogin, mainCtrl.deleteLink)

  app.post('/clientRegister', mainCtrl.clientRegister)

// --------------------------------------------------------------------Catch All--------------------------------------------------------
  app.all('/*', function (req, res) {
    res.render('404.html')
  })
}
