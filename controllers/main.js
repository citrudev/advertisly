
var User = require('../models/user')
var Link = require('../models/link')
var Cars = require('../models/cars')
var Images = require('../models/images')
var mail = require('../lib/email')

module.exports.homeView = function (req, res) {
  res.render('home.html')
}

module.exports.workspaceView = function (req, res) {
  res.render('workspace.html')
}

module.exports.adminView = function (req, res) {
  res.render('admin.html')
}

module.exports.signupStatusView = function (req, res) {
  res.render('signupStatus.html')
}

module.exports.signupFailedView = function (req, res) {
  res.render('signupFailed.html')
}

module.exports.signinFailedView = function (req, res) {
  res.render('signinFailed.html')
}

module.exports.forgotView = function (req, res) {
  res.render('forgot.html')
}

module.exports.signupView = function (req, res) {
  res.render('signup.html')
}

module.exports.loginView = function (req, res) {
  res.render('login.html')
}

module.exports.landingView = function (req, res) {
  res.render('theme/docs/index.html')
}

module.exports.clientView = function (req, res) {
  res.render('theme/docs/clients.html', {plan: req.params.plan})
}

module.exports.getCurrentUser = function (req, res) {
  res.json(req.user)
}

module.exports.getAllUsers = function (req, res) {
  User.find({email: {$ne: 'admin@carpricegrabber.com'}}, {password: 0}, function (err, users) {
    if (err) return res.status(500).end()
    res.json(users)
  })
}

module.exports.updateUserPrivilege = function (req, res) {
  var id = req.body.id
  var verified = req.body.verified
  var isAdmin = req.body.isAdmin
  var isCrowdWorker = req.body.isCrowdWorker

  User.findOne({_id: id}, function (err, user) {
    if (!err && user) {
      user.isCrowdWorker = typeof isCrowdWorker !== 'undefined' ? isCrowdWorker : user.isCrowdWorker
      user.verified = typeof verified !== 'undefined' ? verified : user.verified
      user.isAdmin = typeof isAdmin !== 'undefined' ? isAdmin : user.isAdmin
      user.save(function (err, user) {
        if (err) return res.status(500).end()
        return res.status(200).end()
      })
    } else {
      res.status(500).end()
    }
  })
}

module.exports.deleteUser = function (req, res) {
  User.findOne({_id: req.body.id}, function (err, user) {
    if (!err && user) {
      user.remove(function (err) {
        if (!err) {
          res.status(200).end()
        } else {
          res.status(500).end()
        }
      })
    } else {
      res.status(500).end()
    }
  })
}

module.exports.getAllLinks = function (req, res) {
  Link.find({user: req.user._id}, function (err, links) {
    if (err) return res.status(500).end()
    res.json(links)
  })
}

module.exports.addLink = function (req, res) {
  var newLink = new Link()
  newLink.url = req.body.url
  newLink.user = req.user._id
  newLink.comment = req.body.comment || ''
  newLink.save(function (err) {
    if (err) {
      res.status(500).end()
    } else {
      res.status(200).end()
    }
  })
}

module.exports.deleteLink = function (req, res) {
  Link.findOne({_id: req.body.id}, function (err, link) {
    if (err) return res.status(500).end()
    link.remove(function (err) {
      if (err) {
        res.status(500).end()
      } else {
        res.status(200).end()
      }
    })
  })
}

module.exports.updateLinkComment = function (req, res) {
  Link.findOne({_id: req.body.id}, function (err, link) {
    if (err) return res.status(500).end()
    link.comment = req.body.comment || link.comment
    link.save(function (err) {
      if (!err) return res.status(200).end()
      res.status(500).end()
    })
  })
}

module.exports.addCar = function (req, res) {
  console.log(req.body)
  Cars.findOne({hash: req.body.hash}, function (err, car) {
    if (err) return res.status(500).end()
    if (!car) {
      var newCar = new Cars()
      newCar.url = req.body.url
      newCar.hash = req.body.hash
      newCar.user = req.body.user
      newCar.link = req.body.link
      newCar.img = req.body.img
      newCar.make = req.body.make
      newCar.model = req.body.model
      newCar.trim = req.body.trim
      newCar.year = req.body.year
      newCar.lease = req.body.lease
      newCar.finance = req.body.finance
      newCar.msrp = req.body.msrp
      newCar.save(function (err, car) {
        if (err) {
          console.log(err)

          res.status(500).end()
        } else {
          res.status(200).end()
        }
      })
    } else {
      res.status(200).end()
    }
  })
}

module.exports.getAllCars = function (req, res) {
  Cars.find({user: req.user._id}, function (err, cars) {
    if (err) return res.status(500).end()
    return res.json(cars)
  })
}

module.exports.getAllImages = function (req, res) {
  Images.find({extracted: false}, function (err, images) {
    if (err) return res.status(500).end()
    res.json(images)
  })
}

module.exports.setImageExtracted = function (req, res) {
  Images.findOne({hash: req.body.hash}, function (err, image) {
    if (err) return res.status(500).end()
    if (image) {
      image.extracted = true
      image.save(function (err) {
        if (err) return res.status(500).end()
        res.status(200).end()
      })
    }
  })
}

module.exports.deleteAllImages = function (req, res) {
  Images.remove({}, function (err) {
    if (err) return res.status(500).end()
    res.status(200).end()
  })
}

module.exports.deleteAllCars = function (req, res) {
  Cars.remove({}, function (err) {
    if (err) return res.status(500).end()
    res.status(200).end()
  })
}

module.exports.clientRegister = function (req, res) {
  var plan = req.body.plan
  var name = req.body.name
  var website = req.body.website
  var email = req.body.email
  var dealers = req.body.dealers
  var phone = req.body.phone

  var info = 'Plan: ' + plan + '\n\n' + 'Name: ' + name + '\n\n' + 'Website: ' + website + '\n\n' + 'Email: ' + email + '\n\n' + 'Dealers: ' + dealers + '\n\n' + 'Phone: ' + phone

  var mailOptions = {
    from: 'Carpricegrabber.com <carpricegrabber@gmail.com>',
    to: 'brennan@advertisly.com',
    subject: 'New Client',
    text: info
  }

  mail.sendMail(mailOptions, function (err, response) {
    if (err) {
      console.log(err)
    } else {
      console.log(response)
    }
  })

  res.render('theme/docs/thankyou.html')
}
