var asyncInterval = require('asyncinterval')
var async = require('async')
var vo = require('vo')
var appDebug = require('debug')('scheduler')
var childProcess = require('child_process')
var moduleLoader = require('../lib/moduleLoader')

module.exports = function (interval, timeout, lpi, links) {
  var asyncI = asyncInterval(function (done) {
    appDebug('monitoring')
    links.loadLinks(function (err, dataset) {
      if (!err) {
        async.eachSeries(dataset, function (link, cb) {
          vo(moduleLoader.getScrapePluginFn(link)(link))(function (err, result) {
            if (!err) {
              cb(null)
            } else {
              cb(err)
            }
          })
        })
      }
    })
    done()
  }, interval, timeout)

  asyncI.onTimeout(function () {
    childProcess.exec('killall -9 electron')
  })
}

module.exports.startUp = function (links) {
  appDebug('monitoring')
  links.loadLinks(function (err, dataset) {
    if (!err) {
      async.eachSeries(dataset, function (link, cb) {
        vo(moduleLoader.getScrapePluginFn(link)(link))(function (err, result) {
          if (!err) {
            cb(null)
          } else {
            cb(err)
          }
        })
      })
    }
  })
}
