var Link = require('../models/link')

module.exports = function (link) {
  if (!(link.plugin)) {
    Link.findOne({_id: link._id}, function (err, modLink) {
      if (err) throw err
      if (modLink) {
        modLink.plugin = true
        modLink.save()
      }
    })
  }
}
