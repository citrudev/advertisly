// scaping plugin for lutherhopkinshonda.com
// =====================================START=========================================

var nightmare = require('../lib/configuredNightmare')(require('nightmare'))
var datastore = require('../lib/datastore')
var utility = require('../lib/utility')

var log = []

var scrape = function * (link) {
  var images = yield nightmare.goto('null')
  .wait()
  .inject('js', 'node_modules/jquery/dist/jquery.js')
  .evaluate(function (link) {
    var images = []
    $('.dsbCont').each(function () {
      var imageObj = {}
      imageObj.url = $(this).find('a').attr('href')
      imageObj.img = $(this).find('a img').attr('src')
      imageObj.link = link._id
      imageObj.user = link.user
      if (imageObj.url && imageObj.img) {
        images.push(imageObj)
      }
    })
    return images
  }, link).end()

  datastore.deleteAllImages()
  return {
    log: log,
    link: link
  }
}

// =======================================================PLUGIN====================================================
module.exports['null'] = {
  scrape: scrape
}
