var LocalStrategy = require('passport-local').Strategy
var User = require('../models/user')

module.exports = function (passport) {
  passport.serializeUser(function (user, done) {
    done(null, user._id)
  })

  passport.deserializeUser(function (id, done) {
    User.findById(id, function (err, user) {
      done(err, user)
    })
  })
// configure passport LocalStrategy used to register user
  passport.use('local-register', new LocalStrategy({
    usernameField: 'email',
    passwordField: 'password1',
    passReqToCallback: true
  },
  function (req, email, password, done) {
    process.nextTick(function () {
      User.findOne({email: email}, function (err, user) {
        if (err) return done(err)
        if (user) return done(null, false, {message: 'email already exist'})
        var newUser = new User()
        newUser.email = email
        newUser.password = newUser.generateHash(password)
        newUser.dealership = req.body.dealership || ''
        newUser.save(function (err, user) {
          if (err) return done(err)
          return done(null, user)
        })
      })
    })
  }
))

// configure passport LocalStrategy used to authenticate
  passport.use('local-login', new LocalStrategy({
    usernameField: 'email',
    passwordField: 'password',
    passReqToCallback: true
  }, function (req, email, pass, done) {
    User.findOne({email: email}, function (err, user) {
      if (err) return done(err)
      if (!user) return done(null, false, {message: 'Incorrect username.'})
      if (!user.validPassword(pass)) return done(null, false, {message: 'Incorrect password.'})
      return done(null, user)
    })
  }))

// register a user
  var register = passport.authenticate('local-register', {
    successRedirect: '/signupStatus', // redirect to the status page
    failureRedirect: '/signupFailed' // redirect to status page
  })

// authenticate a user
  var login = passport.authenticate('local-login', {
    successRedirect: '/home',
    failureRedirect: '/signinFailed',
    failureFlash: true
  })

// test authentication
  var isAuthenticated = function (req, res, next) {
    if (!req.isAuthenticated()) return res.redirect('/')
    if (!req.user.verified) return res.redirect('/signupStatus')
// if user not authenticated redirect to login page
    next()
  }

  var isAdmin = function (req, res, next) {
    if (req.user.isAdmin) {
      res.redirect('/admin')
    }
    next()
  }

  var isCrowdWorker = function (req, res, next) {
    if (!req.user.isAdmin && req.user.isCrowdWorker) {
      res.redirect('/workspace')
    }
    next()
  }

  function requireLogin (req, res, next) {
    if (!req.user) {
      req.logout()
      return res.redirect('/')
    }
    next()
  }

  function apiRequireLogin (req, res, next) {
    if (!req.user) {
      req.logout()
      return res.redirect('/')
    }
    next()
  }

  function adminApi (req, res, next) {
    if (req.user.isAdmin) {
      return next()
    }
    req.logout()
    res.redirect('/')
  }

  function workerApi (req, res, next) {
    if (req.user.isCrowdWorker || req.user.isAdmin) {
      return next()
    }
    req.logout()
    res.redirect('/')
  }

  var logout = function (req, res) {
    req.logout()
    res.redirect('/')
  }

  return {
    register: register,
    login: login,
    isAuthenticated: isAuthenticated,
    isAdmin: isAdmin,
    logout: logout,
    isCrowdWorker: isCrowdWorker,
    requireLogin: requireLogin,
    adminApi: adminApi,
    workerApi: workerApi,
    apiRequireLogin: apiRequireLogin

  }
}
